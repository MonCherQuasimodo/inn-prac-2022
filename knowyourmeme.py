import requests
from pathlib import Path
import re
import logging
from bs4 import BeautifulSoup
import json
import os
from time import sleep
from typing import Optional, List, Tuple, Dict

logging.basicConfig(filename='knowyourmeme.log', filemode='w', level=logging.INFO, format='%(asctime)s %(message)s')

log = logging.getLogger(__name__)

##### CONFIG #####

# A path to the collected dataset
DATASET_PATH = \
    Path('./datasets/knowyourmeme')

# A timeout for HTTP requests
TIMEOUT = \
    1

BASE_URL = 'https://knowyourmeme.com/memes/'

# Pages to scrape
PAGES = \
    [f'{BASE_URL}page/{page_number}' for page_number in range(1, 10)]

# Request headers
HEADERS = \
    {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0'
    }

##### END OF CONFIG #####


class Scraper:
    def __init__(self, pages: str = PAGES, dataset_dir: Path = DATASET_PATH):
        self.pages: str = pages
        self.dataset_dir: Path = dataset_dir
        self.title: Optional[str] = None
        self.soup: Optional[BeautifulSoup] = None
        self.tag_ratings: Dict[str, int] = {}

    @staticmethod
    def get_filename(url: str) -> str:
        """
        Function to handle file extensions
        """
        split = url.rsplit('.', 1)
        if len(split[1]) > 4:
            return re.sub(r'[^\w]', '', url)
        else:
            return re.sub(r'[^\w]', '', split[0]) + '.' + split[1]

    def download(self, url: str, prefix: str = '') -> str:
        """
        Download a src file

        @return: A path to the downloaded file
        """
        directory = self.dataset_dir / self.title
        file = directory / (prefix + Scraper.get_filename(url))
        with file.open('wb') as f:
            r = requests.get(url, headers=HEADERS)
            f.write(r.content)
        return str(file)

    def scrape(self):
        if not self.dataset_dir.exists():
            os.mkdir(self.dataset_dir)
            log.info(f'Creating dataset dir at {self.dataset_dir}')

        main_file = self.dataset_dir / 'main.json'
        main_dict = {}

        ratings_file = self.dataset_dir / 'tags.json'

        try:
            for page in self.pages:
                sleep(TIMEOUT)
                try:
                    r = requests.get(url=page, headers=HEADERS)
                    soup = BeautifulSoup(r.content, features='html.parser')
                    log.info(f'Scraping page {page}')

                    for row in soup.body.find('div', id='entries_list').table.tbody.children:
                        if len(row) > 3:
                            for cell in row.children:
                                if len(cell) > 3:
                                    sleep(TIMEOUT)
                                    title = cell.a["href"].split('/')[-1]
                                    main_dict[title] = self.scrape_meme_page(title)
                except Exception as ex:
                    log.exception(ex)
        finally:
            with main_file.open('w') as f:
                json.dump(main_dict, f, indent=4)
            with ratings_file.open('w') as f:
                json.dump(self.tag_ratings, f, indent=4)

    def get_about_info(self) -> Optional[str]:
        about_entry = self.soup.body.find(id='content').div.article.find_all(id='about')
        if len(about_entry) > 1:
            log.info(f'Too many about entries for {self.title}')
            return None
        elif len(about_entry) == 0:
            log.info(f'No about entry for {self.title}')
            return None
        else:
            log.info(f'Added about entry for {self.title}')
            return about_entry[0].parent.p.get_text()

    def get_details(self) -> dict:
        result = {}
        for child in self.soup.body.find(id='content').div.article.header.section.find_all(attrs={"class": "detail"}):
            details = []
            for grandchild in child.contents[3].children:
                if len(grandchild.get_text()) > 3:
                    details.append(grandchild.get_text().replace('\n', ''))
            result[child.contents[1].get_text()] = details
        log.info(f'Added details for {self.title}')
        return result

    def get_main_image(self) -> str:
        src = self.soup.body.find(id='content').div.article.header.find(attrs={"class": "photo-wrapper"}).a.img['data-src']
        path = self.download(src, prefix='main_')
        log.info(f'Downloaded main image for {self.title}')
        return path

    def get_templates(self) -> List[str]:
        template_list = []
        templates = self.soup.body.find(id='content').div.article.find_all(id='template')
        templates.extend(self.soup.body.find(id='content').div.article.find_all(id='templates'))
        if len(templates) > 1:
            log.info(f'Too many template entries for {self.title}')
        elif len(templates) == 0:
            log.info(f'No template entry for {self.title}')
        else:
            for child in templates[0].parent.center.children:
                if len(str(child)) > 3 and len(child.find_all('img')) == 1:
                    src = child.img['data-src']
                    template_list.append(self.download(src, prefix='template_'))
            log.info(f'Added templates for {self.title}')
        return template_list

    def get_examples(self) -> List[str]:
        examples_list = []
        various_examples = self.soup.body.find(id='content').div.article.find_all(id='various-examples')
        if len(various_examples) > 1:
            log.info(f'Too many various examples entries for {self.title}')
        elif len(various_examples) == 0:
            log.info(f'No various examples entry {self.title}')
        else:
            for child in various_examples[0].parent.center.children:
                if len(str(child)) > 3 and len(child.find_all('img')) == 1:
                    src = child.img['data-src']
                    examples_list.append(self.download(src, prefix='example_'))
            log.info('Added various examples')
        return examples_list

    def get_tags(self) -> List[Tuple[str, int]]:
        tags = self.soup.body.find(id='content').div.article.find_all(attrs={"class": "tags"})
        tag_list = []
        if len(tags) > 1:
            log.info(f'Too many tag entries for {self.title}')
        elif len(tags) == 0:
            log.info(f'No tag entry for {self.title}')
        else:
            for child in tags[0].children:
                if len(str(child)) > 3:
                    try:
                        tag_list.append(child.get_text())
                        if self.tag_ratings.get(child.get_text()) is None:
                            self.tag_ratings[child.get_text()] = 0
                        self.tag_ratings[child.get_text()] += 1
                    except Exception as e:
                        log.exception(e)
            log.info(f'Added tags for {self.title}')
        return tag_list

    def scrape_meme_page(self, title: str) -> Optional[dict]:
        self.title = title
        log.info(f'Scraping page {self.title}')
        meme_dir = self.dataset_dir / self.title

        if meme_dir.exists():
            log.warning(f'Directory {meme_dir} already exists')
        else:
            os.mkdir(meme_dir)
            log.info(f'Creating subdir {meme_dir}')

        try:
            page = requests.get(url=BASE_URL + self.title, headers=HEADERS)
            self.soup = BeautifulSoup(page.content, features='html.parser')
        except Exception as e:
            log.exception(e)
            return None

        processed_page = {'title': self.title}

        functions = {
            'about': self.get_about_info,
            'image': self.get_main_image,
            'details': self.get_details,
            'examples': self.get_examples,
            'templates': self.get_templates,
            'tags': self.get_tags
        }

        for key in functions.keys():
            try:
                processed_page[key] = functions[key]()
            except Exception as e:
                log.exception(e)

        log.info(f'End of processing {self.title}')
        return processed_page


if __name__ == '__main__':
    dataset = Scraper().scrape()
